const express = require("express");
const bodyParser = require("body-parser");

const sequelize = require("./utils/database");
const visitRoutes = require("./routes/visit");
const notFoundController = require("./controllers/notFoundController");

const User = require('./model/patient')
const Docor = require('./model/doctor')
User.sync();
Docor.sync();

const app = express();
app.use(bodyParser.json());


app.use(visitRoutes);


app.use(notFoundController.get404);


sequelize
    .sync()
    .then((result) => {
        console.log(result);
        app.listen(3000, () => console.log(`Server is running...`));
    })
    .catch((err) => console.log(err));


module.exports = app;