const { DataTypes } = require("sequelize");

const sequelize = require("../../learn/Training/todolist2/utils/database");

const Patient = sequelize.define("Patient", {

    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    patientNumber: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    doctorNumber: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    ticketNumber: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },

});


module.exports = Patient;
