const { DataTypes } = require("sequelize");

const sequelize = require("../utils/database");

const Doctor = sequelize.define("Doctor", {
   
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    doctorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },  
    nezamCode: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    maxVisit: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    todayVisit: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
  
});

module.exports = Doctor;
