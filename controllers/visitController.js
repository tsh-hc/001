
const Doctor = require("../model/doctor");
const Patient = require("../model/patient");

exports.newTicket = async (req, res) => {

    if (!Number.isInteger(req.body.userId)) {
        return res.json({ message: 'لطفا کد کاربری صحیح وارد فرمایید', ticketNumber: 0 });
    }
    try {
        const doctor = await Doctor.findOne(
            {
                where: {
                    doctorId: req.body.doctorId,
                }
            })
        const patientOld = await Patient.findAll(
            {
                where: {
                    patientNumber: req.body.userId,
                    doctorNumber: req.body.doctorId
                }
            })

        if (patientOld.length > 0) {
            return res.json({ message: 'شما قبلا نوبت گرفته اید', ticketNumber: patientOld[0].ticketNumber });
        } else {

            if (doctor.todayVisit <= doctor.maxVisit) {

                const ticket = Math.floor(Math.random() * 100000);
                await Patient.create({
                    patientNumber: req.body.userId,
                    doctorNumber: req.body.doctorId,
                    ticketNumber: ticket
                });
                doctor.todayVisit += 1
                await doctor.save();
                res.json({ message: 'درخواست شما با موفقیت ثبت شد', ticketNumber: ticket });
            }
            else {
                res.json({ message: 'متاسفانه نوبت دهی در روز جاری امکان پذبر نیست', ticketNumber: 0 });
            }
        }
    } catch (err) {
        console.log(err);
    }
};

//تعریف اولین پزشک با متغیرهای پیش فرض 
//(باتوجه به این که در درخواست دریافتی مخاطب همیشه یک پزشک است)

exports.addNewDoctor = async (req, res) => {
    try {
        await Doctor.create({
            name: "dr TEST",
            doctorId: 1,
            nezamCode: 12345,
            maxVisit: 25,
            todayVisit: 0
        });

        res.json({ message: 'درخواست شما با موفقیت ثبت شد'});

    } catch (err) {
        console.log(err);
    }
};



