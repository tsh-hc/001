const express = require("express");

const visitController = require("../controllers/visitController");

const router = express.Router();

router.post("/getTicket", visitController.newTicket);
router.post("/add", visitController.addNewDoctor);



module.exports = router;
