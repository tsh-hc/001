const should = require('chai').should();
const sequelize = require("../utils/database");
const Patient = require('../model/patient');

process.env.NODE_ENV = 'test';

describe('Patient DB Tests', () => {

    let body = {
        patientNumber: 1001,
        doctorNumber: 1,
        ticketNumber: 1001
    };

    it('DB Connection', async () => {
        await sequelize.sync()
            .then('open', () => done())
            .catch('error', (error) => {
                console.log(error);
            });
    });

    it('Add Patient', async () => {
        let patient = new Patient(body);
        patient = await patient.save();

        patient.should.be.a('object');
        patient.should.have.property('patientNumber');
        patient.should.have.property('doctorNumber');
        patient.should.have.property('ticketNumber');
      
    });

    it('Find Patient', async () => {
        let patient = await Patient.findAll({});

        patient.should.be.a('array');
    });

    it('Update Patient', async () => {
        let patient = await Patient.findOne({
            where: {
                ticketNumber: 1001
            }
        });
        let result = await patient.update({
            ticketNumber: 2001
        })
      
        result.should.be.a('object');
        result.dataValues.ticketNumber.should.be.eql(2001);
    });

    it('Remove Patient', async () => {

        let result = await Patient.destroy({
            where: {
                patientNumber: 1001
            }
        });
     
        result.should.be.eql(1);
    });

});
