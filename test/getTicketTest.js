const Doctor = require('../model/doctor');
const Patient = require('../model/patient');

const chai = require('chai');
const should = require('chai').should();
const chaiHttp = require('chai-http');
const server = require('../server');

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);


describe('/getTicket', () => {


    let passReq = {
        userId: 123000123,
        doctorId: 1
    };

    let wrongReq = {
        userId: 123000123.1,
        doctorId: 1
    };

    let patientMaxVisit = {
        userId: 1230000123,
        doctorId: 2
    };

    before('Create Doctor For Max Visit', async () => {
        await Doctor.create({
            name: "DOCTOR FOR MAX VISIT",
            doctorId: 2,
            nezamCode: 123000123,
            maxVisit: 15,
            todayVisit: 16
        });
	});

    it('Not accepted because wrong userId', (done) => {
        chai.request(server)
            .post('/getTicket')
            .send(wrongReq)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('لطفا کد کاربری صحیح وارد فرمایید');
                res.body.should.have.property('ticketNumber').eql(0);
                done();
            });
    });

    it('accepted request', (done) => {
        chai.request(server)
            .post('/getTicket')
            .send(passReq)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('درخواست شما با موفقیت ثبت شد');
                res.body.should.have.property('ticketNumber');
                done();
            });
    });

    it('Not accepted because repeated userId', (done) => {
        chai.request(server)
            .post('/getTicket')
            .send(passReq)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('شما قبلا نوبت گرفته اید');
                res.body.should.have.property('ticketNumber');
                done();
            });
    });

    it('Not accepted because maxVisit', (done) => {
            chai.request(server)
                .post('/getTicket')
                .send(patientMaxVisit)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('متاسفانه نوبت دهی در روز جاری امکان پذبر نیست');
                    res.body.should.have.property('ticketNumber').eql(0);
                    done();
                });
    });
    

    after('Delete Test Patient', async () => {
        const doctor = await Doctor.findOne(
            {
                where: {
                    doctorId: 1,
                }
            })
        doctor.todayVisit -= 1
        await doctor.save();

        await Doctor.destroy({
            where: {
                name: "DOCTOR FOR MAX VISIT"
            }
        });
        await Patient.destroy({
            where: {
                patientNumber: 123000123
            }
        });
    });
});
