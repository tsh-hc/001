const should = require('chai').should();
const sequelize = require("../utils/database");
const Doctor = require('../model/doctor');

process.env.NODE_ENV = 'test';

describe('Doctor DB Tests', () => {

    let body = {
        name: "TEST NAME DOCTOR",
        doctorId: 2,
        nezamCode: 54321,
        maxVisit: 15,
        todayVisit: 0
    };

    it('DB Connection', async () => {
        await sequelize.sync()
            .then('open', () => done())
            .catch('error', (error) => {
                console.log(error);
            });
    });

    it('Add Doctor', async () => {
        let doctor = new Doctor(body);
        doctor = await doctor.save();

        doctor.should.be.a('object');
        doctor.should.have.property('name');
        doctor.should.have.property('doctorId');
        doctor.should.have.property('nezamCode');
        doctor.should.have.property('maxVisit');
        doctor.should.have.property('todayVisit');
    });

    it('Find Doctor', async () => {
        let doctors = await Doctor.findAll({});

        doctors.should.be.a('array');
    });

    it('Update Doctor', async () => {
        let doctor = await Doctor.findOne({
            where: {
                name: "TEST NAME DOCTOR"
            }
        });
        let result = await doctor.update({
            nezamCode: 98765
        })
      
        result.should.be.a('object');
        result.dataValues.nezamCode.should.be.eql(98765);
    });

    it('Remove Doctor', async () => {

        let result = await Doctor.destroy({
            where: {
                name: "TEST NAME DOCTOR"
            }
        });
        result.should.be.eql(1);
    });

});
