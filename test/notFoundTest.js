const chai = require('chai');
const should = require('chai').should();
const chaiHttp = require('chai-http');
const server = require('../server');

process.env.NODE_ENV = 'test';

chai.use(chaiHttp);

describe('Not Found Page', () => {
    it('Not yet Available', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('در حال حاضر چنین خدمتی وجود ندارد');
                done();
            });
    });
});
